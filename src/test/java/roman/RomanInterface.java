package roman;

import static org.junit.Assert.*;
import org.junit.Test;
import roman.RomanConverter;

public class RomanInterface {
	
	RomanConverter roman = new RomanConverter();
	
	@Test
	public void test() {
		//fail("Not yet implemented");

		String r = " ";

		try{
			roman.fromRoman(r);
		} catch( final IllegalArgumentException e ){
		    final String msg = "Invalid Roman numeral: "+r;
		    assertEquals(msg, e.getMessage());
		 }
		
		try{
			roman.toRoman(123456);
			roman.toRoman(0);
			roman.toRoman(-3);
		} catch( final IllegalArgumentException e ){
		    final String msg = "number out of range (must be 1..3999)";
		    assertEquals(msg, e.getMessage());
		 }
	}
}
