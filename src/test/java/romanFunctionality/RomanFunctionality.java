package romanFunctionality;

import static org.junit.Assert.*;

import org.junit.Test;

import roman.RomanConverter;

public class RomanFunctionality {

	@Test
	public void test() {
		//fail("Not yet implemented");
		RomanConverter roman = new RomanConverter();
		
		assertEquals(5, roman.fromRoman("V"));
		assertEquals("V", roman.toRoman(5));
		
			//take a number, convert it to Roman numerals, 
			//then convert that back to a number, 
			//we should end up with the number we started with	
				for (int i = 1; i < 4000; i++)
				{
					String str = roman.toRoman(i);
					int rom = roman.fromRoman(str);
					assertEquals(i,rom);	
				}
	}

}
